package comp2019_Assignment1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;


/**
 * This class finds the best path from a start location to the goal (HQ) location given the map.
 * Each location along the path, including start and end location, must have a terrain value less than or equal to a given threshold.
 * The entry point for your code is in method findPath().
 *
 * DO NOT MODIFY THE SIGNATURE OF EXISTING METHODS AND VARIABLES.
 * Otherwise, JUnit tests will fail and you will receive no credit for your code.
 * Of course, you can add additional methods and classes in your implementation.
 * 
 * @author Carmen Grantham
 *
 *
 * Note: Fails testWorld04 - Explored too many states: 7225 not in range [7205,7209]
 *       All other tests pass
 *
 */
public class PathFinder {

    protected Location start;		// start location
    protected Location goal;		// goal location
    protected RectangularMap map;   // the map
    protected int terrainThreshold; // threshold for navigable terrain

    private int explored = 0;	    //  number of states visited during the search;
                                    // increment this whenever you remove a state from the frontier
    
    private int lowestCost = Integer.MAX_VALUE; // The lowest step cost on the map, used in heuristic function

    public PathFinder(RectangularMap map, Location start, Location goal, int terrainThreshold) {
        this.map = map;
        this.start = start;
        this.goal = goal;
        this.terrainThreshold = terrainThreshold;
        this.lowestCost = getLowestCost();
    }

    public RectangularMap getMap() {
        return map;
    }

    public Location getStart() {
        return start;
    }

    public Location getGoal() {
        return goal;
    }

    public int getTerrainThreshold() {
        return terrainThreshold;
    }

    public int getExplored() {
        return explored;
    }

	/* DO NOT CHANGE THE CODE ABOVE */
    /* adding imports and variables is okay. */
    /* TODO: add your code below. you can add extra methods. */

    public Path findPath() {
        //
        //TODO Question1
        // Implement A* search that finds the best path from start to goal.
        // Return a Path object if a solution was found; return null otherwise.
        // Refer to the assignment specification for details about the desired path.

        // Can only pass terrain for start node when it's under the terrain threshold
        int startTerrain = map.getValueAt(start);
        if (startTerrain > terrainThreshold) {
            return null;
        }
        
        // The search frontier with lowest cost locations at front of the queue
        Queue<SearchState> open = new PriorityQueue<SearchState>(10);
        
        // Track Location objects already in "open" queue
        // Same contents as open Queue but with an easier way to check location exists
        Set<Location> openLocations = new HashSet<Location>();
        
        // Locations already explored
        Map<Location, SearchState> closed = new Hashtable<Location, SearchState>();
        
        // The starting search state object with cost of 0
        SearchState startSearchState = new SearchState(start, 0, getEstimatedCost(start, goal));
        
        // Add start location to frontier
        open.add(startSearchState);
        openLocations.add(start);
        
        
        while (!open.isEmpty()) {
            // Get and remove location with least cost.
            SearchState current = open.poll();
            openLocations.remove(current.getLocation());
            
            explored++;
            
            // If goal is found return current path
            if (current.getLocation().equals(goal)) {
                return current.getPath();
            }

            // Mark current location as explored
            closed.put(current.getLocation(), current);
            
            // Process all neighbours of current location to be explored next
            for (Location neighbour : map.getNeighbours(current.getLocation())) {
                
                // If neighbour already explored, skip 
                if (closed.containsKey(neighbour)) {
                    continue;
                }
                
                // If terrain exceeds threshold  do not explore
                int neighbourTerrain = map.getValueAt(neighbour);
                if (neighbourTerrain > terrainThreshold) {
                    // If neighbour is goal and it's terrain exceeds threshold then it's impossible to solve
                    if (neighbour.equals(goal)) {
                        return null;
                    }
                    continue;
                }
                
                // Calculate step costs
                int costToNeighbour = getCostToNeighbor(current.getLocation(), neighbour);
                int costFromStart = current.getPath().getCosts() + costToNeighbour;
                int estimatedCostForNeighbour = getEstimatedCost(neighbour, goal);
                int estimatedPathCost = costFromStart + estimatedCostForNeighbour;
                
                
                boolean addNeighbour = false;
                
                // If neighbour not in closed or open list then add neighbour
                if (!closed.containsKey(neighbour) && !openLocations.contains(neighbour)) {
                    addNeighbour = true;
                } else if (openLocations.contains(neighbour)) {
                    // Neighbour is in open list.
                    SearchState neighbourState = getNode(open, neighbour);
                    
                    // If estimated cost of this new neighbour is less than existing cost for location
                    // delete it from the open list and add the new neighbour
                    if (estimatedPathCost < neighbourState.getF()) {
                        open.remove(neighbourState);
                        openLocations.remove(neighbour);

                        addNeighbour = true;
                    }
                }
                
                if (addNeighbour) {
                    // Add the neighbour to the open list.
                    SearchState state = new SearchState(current, neighbour, costFromStart, estimatedCostForNeighbour);
                    open.add(state);
                    openLocations.add(neighbour);
                }
            }
        }
  
    	// no solution found
        return null;
    }
    
    
    /**
     * Get the cost to get from neighbouring locations, which is the value of the two
     * locations added together
     * @param start The starting location
     * @param neighbour The neighbour
     * @return Cost to get from location to it's neighbour
     */
    protected int getCostToNeighbor(Location start, Location neighbour) {
        return map.getValueAt(start) + map.getValueAt(neighbour);
    }
    
    /**
     * The heuristic to estimate cost from source location to goal location.
     * @param source The source location
     * @param goal The goal location
     * @return Estimated cost between source and goal
     */
    protected int getEstimatedCost(Location source, Location goal) {
        // Use Manhattan distance multiplied by the lowest step cost on the board 
        int rowDiff = Math.abs(source.getRow() - goal.getRow());
        int columnDiff = Math.abs(source.getColumn() - goal.getColumn());
      
        return (rowDiff + columnDiff) * lowestCost;
    }
    
    /**
     * Get the lowest step cost on the board
     * @return Lowest step cost
     */
    private int getLowestCost() {
        int lowest = Integer.MAX_VALUE;
        
        // Loop through all rows and columns to find the lowest step cost
        for (int row = 0; row < map.getRows(); row++) {
            for (int column = 0; column < map.getColumns(); column++) {
                int value = map.getValueAt(row, column);
                if (value < lowest) {
                    lowest = value;
                }
            }
        }
        return lowest;
    }
    
    /**
     * Get the node in the SearchState queue with this location.
     * @param queue The queue to use.
     * @param loc The location to use
     * @return Node in SearchState queue with this location.
     */
    private SearchState getNode(Queue<SearchState> queue, Location loc) {
        for (SearchState state : queue) {
            if (state.getLocation().equals(loc)) {
                return state;
            }
        }
        return null;
    }

    /**
     * Increase explored counter by 1.
     */
    protected void incrementExplored() {
        explored++;
    }
}
