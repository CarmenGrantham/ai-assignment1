package comp2019_Assignment1;

import java.util.Hashtable;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * 
 * @author Carmen Grantham
 * 
 * Note. Fails testWorld04 Path incorrect. My result has 2 nodes incorrect and after considerable time 
 *       reviewing and debugging cannot find where I have gone wrong.
 *       
 *       Expected:  (48,43)(49,43)(50,43)(50,42)(50,41)(51,41)(51,40)(51,39)(52,39)(53,39)(53,38)
 *       Actual:    (48,43)(49,43)(49,42)(50,42)(50,41)(50,40)(51,40)(51,39)(52,39)(53,39)(53,38)
 *
 */
public class SafePathFinder extends PathFinder {

    protected RectangularMap enemyMap;  // shows the probability of being captured at each square
    protected double survivalThreshold; // the minimum probability of survival that is required

    public SafePathFinder(RectangularMap map, RectangularMap enemyMap, Location start, Location goal,
                          int terrainThreshold, double survivalThreshold) {
        super(map, start, goal, terrainThreshold);
        this.enemyMap = enemyMap;
        this.survivalThreshold = survivalThreshold;
    }

   	/* DO NOT CHANGE THE CODE ABOVE */
    /* adding imports and variables is okay. */
    
    /* TODO: Question 2
     * add your code below. you can add extra methods. 
     * */

    @Override
    public Path findPath() {

        // Can only pass terrain for start node when it's under the terrain threshold
        int startTerrain = map.getValueAt(start);
        if (startTerrain > terrainThreshold) {
            return null;
        }
        
        // Can only pass enemy for start node when it's under the survival threshold
        double startSurvivalThreshold = getSurvivalProbability(start);
        if (startSurvivalThreshold < survivalThreshold) {
            return null;
        }
        
        // The search frontier with lowest cost locations at front of the queue
        Queue<SearchState> open = new PriorityQueue<SearchState>(10);
        
        // Store SearchState with lowest cost for location
        Map<Location, SearchState> lowestCostStates = new Hashtable<Location, SearchState>();
        
        // Store SearchState with the highest survival probability for a location
        Map<Location, SearchState> highestSurvivalStates = new Hashtable<Location, SearchState>();

        // The starting search state object with cost of 0
        SearchState startSearchState = new SearchState(start, 0, getEstimatedCost(start, goal), startSurvivalThreshold);
        
        // Add start location to frontier
        open.add(startSearchState);
        
        while (!open.isEmpty()) {
            // Get and remove location with least cost.
            SearchState current = open.poll();
            incrementExplored();
            
            // If goal is found return current path
            if (current.getLocation().equals(goal)) {
                return current.getPath();
            }

            // Process all neighbours of current location to be explored next
            for (Location neighbour : map.getNeighbours(current.getLocation())) {
                                
                // If neighbour already in path for current search, skip
                if (current.inPath(neighbour)) {
                    continue;
                }
                    
                // If terrain exceeds threshold  do not explore
                int neighbourTerrain = map.getValueAt(neighbour);
                if (neighbourTerrain > terrainThreshold) {
                    // If neighbour is goal and it's terrain exceeds threshold then it's impossible to solve
                    if (neighbour.equals(goal)) {
                        return null;
                    }
                    continue;
                }                                

                // If neighbour's survival probability exceeds survival threshold do not explore
                double neighbourSurvivalProbability = getSurvivalProbability(current.getSurvivalProbability(), neighbour);
                if (neighbourSurvivalProbability < survivalThreshold) {
                    continue;
                }
                
                // Calculate step costs
                int costToNeighbour = getCostToNeighbor(current.getLocation(), neighbour);
                int costFromStart = current.getPath().getCosts() + costToNeighbour;
                int estimatedCostForNeighbour = getEstimatedCost(neighbour, goal);
                int estimatedPathCost = costFromStart + estimatedCostForNeighbour;
                
                
                boolean addLowerCostNeighbour = false;
                boolean addHigherSurvivalNeighbour = false;
                
                
                SearchState lowestCostState = lowestCostStates.get(neighbour);
                SearchState highestSurvivalState = highestSurvivalStates.get(neighbour);
                
                if (lowestCostState != null && highestSurvivalState != null) {
                    // Determine what should happen with open list in regards to the new neighbour.
                    
                    if (lowestCostState.getF()  > estimatedPathCost) {
                        if (highestSurvivalState.getSurvivalProbability() < neighbourSurvivalProbability) {
                            // New neighbour has lower cost and higher survival probability
                            // >>> Only need one record in open list
                            open.remove(lowestCostState);
                            open.remove(highestSurvivalState);
                            
                            lowestCostStates.remove(neighbour);
                            highestSurvivalStates.remove(neighbour);
                            
                            addLowerCostNeighbour = true;
                            
                        } else {
                            // New neighbour has lower cost but lower or the same survival probability
                            // >>> Replace low cost node and keep higher surviver
                            open.remove(lowestCostState);
                            
                            lowestCostStates.remove(neighbour);
                            
                            addLowerCostNeighbour = true;
                        }
                    } else {
                        if (highestSurvivalState.getSurvivalProbability() < neighbourSurvivalProbability) {
                            // New neighbour has higher survival probability, but higher cost
                            // >>> Replace higher surviver, but keep lower cost
                            open.remove(highestSurvivalState);
                            
                            highestSurvivalStates.remove(neighbour);
                            
                            addHigherSurvivalNeighbour = true;
                        }
                    }
                } else if (lowestCostState != null) {
                    
                    if (lowestCostState.getF()  > estimatedPathCost) {
                        // New neighbour has a lower cost
                        // >> Replace lowest cost
                        open.remove(lowestCostState);
                        
                        lowestCostStates.remove(neighbour);
                        
                        addLowerCostNeighbour = true;
                    } else if (lowestCostState.getF() == estimatedPathCost 
                            && lowestCostState.getSurvivalProbability() < neighbourSurvivalProbability) {
                        // Costs are the same but new neighbour has higher survival probability
                        // >>> Replace lowest cost
                        open.remove(lowestCostState);
                        
                        lowestCostStates.remove(neighbour);
                        
                        addLowerCostNeighbour = true;
                        
                    } else if (lowestCostState.getSurvivalProbability() < neighbourSurvivalProbability) {
                        // New neighbour has higher survival probability, but higher cost
                        // >>> Add new neighbour
                        addHigherSurvivalNeighbour = true;
                    }
                } else {
                    // Neighbour doesn't exist in open list, add it
                    addLowerCostNeighbour = true;
                }
                
                
                if (addLowerCostNeighbour || addHigherSurvivalNeighbour) {
                    // Add new neighbour to open list.
                    SearchState neighbourState = new SearchState(current, neighbour, costFromStart, estimatedCostForNeighbour, neighbourSurvivalProbability);
                    open.add(neighbourState);
                    
                    if (addLowerCostNeighbour) {
                        lowestCostStates.put(neighbour, neighbourState);
                    }
                    
                    if (addHigherSurvivalNeighbour) {
                        highestSurvivalStates.put(neighbour, neighbourState);
                    }
                }
            }
        }
  
        // no solution found
        return null;
    }

     /**
      * Get the survival probability of the location 
      * @param loc The location to use
      * @return Survival probability of the location
      */
     private double getSurvivalProbability(Location loc) {
        double enemy = enemyMap.getValueAt(loc);
        return  1 - (enemy / 100);
    }
    
    /**
     * Calculate the survival probability of a location by multiplying it by
     * it's parent survival probability 
     * @param parentProbability Parents survival probability
     * @param loc The location to use
     * @return Overall survival probability of a location
     */
    private double getSurvivalProbability(double parentProbability, Location loc) {
        return parentProbability * getSurvivalProbability(loc);
    }

}